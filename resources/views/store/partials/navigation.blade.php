<div class="navigation-wrapper">
    <div class="container">
        <!-- Navigation -->
        <nav class="navigation closed clearfix">
            <a href="#" class="menu-toggle-close btn"><i class="fa fa-times"></i></a>
            <ul class="nav sf-menu">
                <li class="active"><a href="{{route('store.home')}}">Home</a></li>
                @if ($categories)
                    @foreach($categories as $cat)
                        <li><a href="{{route('store.category', ['id' => $cat->id])}}">{{$cat->name}}</a></li>
                    @endforeach
                @endif
            </ul>
        </nav>
        <!-- /Navigation -->
    </div>
</div>