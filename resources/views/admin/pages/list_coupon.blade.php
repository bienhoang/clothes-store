@extends('admin.layouts.master')
@section('content')
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">
                        <h2>Quản lý mã giảm giá</h2>
                    </div>
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-8 col-md-12">
                    <div class="card">
                        <div class="header">
                            <h2>Danh sách mã giảm giá</h2>
                        </div>
                        <div class="body table-responsive">
                            <table class="table table-hover m-b-0">
                                <tbody>
                                <thead>
                                    <td>No.</td>
                                    <td>Tên mã</td>
                                    <td>Code</td>
                                    <td>Type</td>
                                    <td>Ngày hết hạn</td>
                                    <td>Số lượng</td>
                                    <td>Sale Off</td>
                                <td>Hành động</td>
                                </thead>
                                @foreach($coupons as $coupon)
                                <tr>
                                    <td>{{$coupon->id}}</td>
                                    <td>{{$coupon->name}}</td>
                                    <td>{{$coupon->code}}</td>
                                    <td>{{$coupon->type}}</td>
                                    <td>{{$coupon->exp_date}}</td>
                                    <td>{{$coupon->total}}</td>
                                    <td>{{$coupon->sale_off}}</td>
                                    <td>
                                        <a href="{{route('admin.update_category_get', ['id' =>''])}}">
                                            <button type="button" class="btn btn-info" title="Edit"><i class="fa fa-edit"></i></button>
                                        </a>
                                        <a href="{{route('admin.delete_category', ['id' => ''])}}">
                                            <button type="button" data-type="confirm" class="btn btn-danger js-sweetalert" title="Delete"><i class="fa fa-trash-o"></i></button>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="card">
                        <div class="header">
                            <h2>Thêm chuyên mã giảm giá</h2>
                        </div>
                        <div class="body">
                            <form id="basic-form" method="post" novalidate="" action="{{route('admin.add_coupon')}}">
                                @csrf
                                <div class="form-group">
                                    <label>Tên mã</label>
                                    <input type="text" class="form-control" required="" name="name">
                                </div>
                                <div class="form-group">
                                    <label>Code</label>
                                    <input type="text" class="form-control" required="" name="code">
                                </div>
                                <div class="form-group">
                                    <label>Type</label>
                                    <select name="type" id="" class="form-control" >
                                        <option value="1">Theo giá</option>
                                        <option value="2">Theo phần trăm</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Số lượng</label>
                                    <input type="text" class="form-control" required="" name="total">
                                </div>
                                <div class="form-group">
                                    <label>Ngày hết hạn</label>
                                    <input type="date" class="form-control" required="" name="exp-date">
                                </div>
                                <div class="form-group">
                                    <label>Sale Off</label>
                                    <input type="text" class="form-control" required="" name="sale-off">
                                </div>
                                <div class="form-group">
                                    <label>Điều kiện đơn hàng</label>
                                    <input type="text" class="form-control" required="" name="condition">
                                </div>
                                <button type="submit" class="btn btn-warning"><i class="fa fa-plus-square"></i><span>   Thêm mã giảm giá</span></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
