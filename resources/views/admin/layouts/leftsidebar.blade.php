<div id="leftsidebar" class="sidebar">
    <div class="sidebar-scroll">
        <nav id="leftsidebar-nav" class="sidebar-nav">
            <ul id="main-menu" class="metismenu">
                <li class="active"><a href="/"><i class="icon-home"></i><span>Dashboard</span></a></li>
                <li class="heading">Products</li>
                <li><a href="{{route('admin.list_product')}}"><i class="icon-envelope"></i><span>List Product</span></a></li>
                <li><a href="{{route('admin.add_product')}}"><i class="icon-bubbles"></i><span>Add Product</span></a></li>
                <li><a href="{{route('admin.list_category')}}"><i class="icon-calendar"></i><span>List Category</span></a></li>
                <li><a href="{{route('admin.list_category')}}"><i class="icon-calendar"></i><span>Add Category</span></a></li>
                <li><a href="{{route('admin.list_order')}}"><i class="icon-notebook"></i><span>Orders</span></a></li>
                <li><a href="{{route('admin.list_coupon')}}"><i class="icon-notebook"></i><span>Coupon</span></a></li>
            </ul>
        </nav>
    </div>
</div>