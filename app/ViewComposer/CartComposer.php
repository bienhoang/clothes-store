<?php

namespace App\ViewComposer;


use App\Model\Coupon;
use Illuminate\View\View;

class CartComposer
{
	/**
	 * Bind data to the view.
	 *
	 * @param  View  $view
	 * @return void
	 */
	public function compose(View $view)
	{
		$cart['product'] = session()->get('cart');
		$totalPrice = 0;

		if (!$cart['product']) {
			$cart['product'] = [];
		}

		foreach ($cart['product'] as $product) {
			$totalPrice += $product['price'] * $product['quantity'];
		}

		$couponCode = session()->get('coupon');
		$coupon = Coupon::where('code', $couponCode)->first();

		if ($coupon && $coupon->exp_date->gt(date('Y-m-d')) && $totalPrice >= $coupon->condition) {
			switch ($coupon->type) {
				case 1:
					$totalPrice = $totalPrice - $coupon->sale_off;
					$cart['sale'] = $coupon->sale_off;
					break;
				case 2:
					$totalPrice = $totalPrice - ($totalPrice * $coupon->sale_off)/100;
					$cart['sale'] = $coupon->sale_off . '%';
					break;
			}

			$cart['coupon'] = $coupon->code;
		} else{
			$cart['sale'] = 0;
		}

		$cart['price'] = $totalPrice;

		$view->with('cart', $cart);
	}
}
