<?php

namespace App\Http\Controllers\Store;


use App\Http\Controllers\Controller;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
	public function index()
	{
		$userName = "Jino Hoang";

		Mail::to('bien@deha-soft.com')->send(new WelcomeMail($userName));

		return view('store.pages.index');
	}
}