<?php

namespace App\Http\Controllers\Store;

use App\Model\Coupon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CouponController extends Controller
{
	public function apply(Request $request)
	{
		$coupon = Coupon::where('code', $request->input('coupon'))->first();

		if (!$coupon || $coupon->exp_date->lt(date('Y-m-d'))) {
			$message = [
				'status' => false,
				'message' => 'Your coupon not exist or has expired'
			];

			return response()->json($message);
		}

		session()->put('coupon', $coupon->code);

		$message = [
			'status' => true,
		];

		return response()->json($message);
	}
}
