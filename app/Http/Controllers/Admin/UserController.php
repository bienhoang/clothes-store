<?php

namespace App\Http\Controllers\Admin;

use App\Model\Role;
use App\User;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
	public function index()
	{
		$users = User::where('deleted', 0)->get();
		$roles = Role::all();

		return view('admin.pages.list_user', ['users' => $users, 'roles' => $roles]);
    }
}
